###We want you to implement:
* An API that allows mobile clients to retrieve the suburb information by
 postcode.
 
* An API that allows mobile clients to retrieve a postcode given a suburb
 name
 
* A secured API to add new suburb and postcode combinations (you'll have
  to work out how this should work)
* Some form of persistence

### Requirements to build and run the solution
* Java 8
* Maven

###Tests
```
cd auspost
mvn clean surefire-report:report
```
report will be at /target/site/surefire-report.html

###Build
```
cd auspost
mvn clean package
java -jar target/auspost-1.0-SNAPSHOT.jar
```

###Run
http://localhost:8080/swagger-ui.html

http://localhost:8080/actuator/health

application will print a valid token to use with secure endpoints on startup (Authorization Header)

```token = eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhdXNwb3N0IiwiZXhwIjoxNTUzOTI1MzY0LCJpYXQiOjE1NTM5MjQ0NjQsInVzZXJJZCI6MX0.Wz_j0jN64lI0SOU7X4RAMJ2NjWlP2wM0Pir9pH3YB0o```


###Example
add suburb

```curl -X POST "http://localhost:8080/api/v1/suburbs" -H "accept: application/json" -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhdXNwb3N0IiwiZXhwIjoxNTUzOTI1MzY0LCJpYXQiOjE1NTM5MjQ0NjQsInVzZXJJZCI6MX0.Wz_j0jN64lI0SOU7X4RAMJ2NjWlP2wM0Pir9pH3YB0o" -H "Content-Type: application/json" -d "{ \"name\": \"maidstone\", \"postCode\": \"3012\"}"```

get by name

```curl -X GET "http://localhost:8080/api/v1/suburbs/name/maidstone" -H "accept: application/json"```

get by postcode

```curl -X GET "http://localhost:8080/api/v1/suburbs/postcode/3012" -H "accept: application/json"```