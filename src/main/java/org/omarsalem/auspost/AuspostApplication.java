package org.omarsalem.auspost;

import org.omarsalem.auspost.security.JWTTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableAspectJAutoProxy
public class AuspostApplication {

    @Autowired
    private JWTTokenService jwtTokenService;

    public static void main(String[] args) {
        SpringApplication.run(AuspostApplication.class, args);
    }


    @PostConstruct
    public void printToken() {
        System.out.println("token = " + jwtTokenService.createToken(1l));
    }
}
