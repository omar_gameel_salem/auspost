package org.omarsalem.auspost.exceptions;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public abstract class ClientException extends Exception {
    protected final List<String> errors;

    protected ClientException(List<String> errors) {
        this.errors = errors;
    }

    public ClientException(String error) {

        this.errors = Arrays.asList(error);
    }
}
