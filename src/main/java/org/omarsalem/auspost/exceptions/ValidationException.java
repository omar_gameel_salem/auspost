package org.omarsalem.auspost.exceptions;

import java.util.List;

public class ValidationException extends ClientException {
    public ValidationException(List<String> errors) {
        super(errors);
    }

    public ValidationException(String message) {
        super(message);
    }
}
