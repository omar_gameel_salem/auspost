package org.omarsalem.auspost.exceptions;

import java.util.List;

public class NotFoundException extends ClientException {
    public NotFoundException(List<String> errors) {
        super(errors);
    }

    public NotFoundException(String error) {
        super(error);
    }
}
