package org.omarsalem.auspost.exceptions;

public class AuthenticationException extends RuntimeException {

    public AuthenticationException(String error) {
        super(error);
    }
}
