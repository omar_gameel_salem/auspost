package org.omarsalem.auspost.security;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.omarsalem.auspost.exceptions.AuthenticationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.Date;

@Service
public class JWTTokenServiceImpl implements JWTTokenService {
    private static final int EXPIRY_VALUE_MILLI_SECONDS = 15 * 60 * 1000;
    private static final JWSAlgorithm ALGORITHM = JWSAlgorithm.HS256;
    private static final JWSHeader HEADER = new JWSHeader(ALGORITHM);
    private static final String USER_ID = "userId";
    private final JWSSigner signer;
    private final JWSVerifier verifier;

    public JWTTokenServiceImpl(@Value("${jwt.secret}") String secret) {
        try {
            signer = new MACSigner(secret);
            verifier = new MACVerifier(secret);
        } catch (JOSEException e) {
            throw new RuntimeException("failed to initialize signer or verifier: ", e);
        }
    }

    @Override
    public String createToken(Long userId) {
        final long now = System.currentTimeMillis();
        final JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .issuer("auspost")
                .issueTime(new Date(now))
                .claim(USER_ID, userId)
                .expirationTime(new Date(now + EXPIRY_VALUE_MILLI_SECONDS))
                .build();

        final SignedJWT signedJWT = new SignedJWT(HEADER, claimsSet);
        try {
            signedJWT.sign(signer);
        } catch (JOSEException e) {
            throw new RuntimeException(e);
        }
        return signedJWT.serialize();

    }

    @Override
    public JWTToken verifyToken(String s) throws AuthenticationException {
        JWTClaimsSet claimsSet;
        try {
            if (StringUtils.isEmpty(s)) {
                throw new AuthenticationException("empty token");
            }
            SignedJWT signedJWT = SignedJWT.parse(s);
            if (!signedJWT.verify(verifier)) {
                throw new AuthenticationException("Tampered token");
            }
            claimsSet = signedJWT.getJWTClaimsSet();
        } catch (JOSEException | ParseException e) {
            throw new AuthenticationException("Tampered token");
        }

        final Long userId = (Long) claimsSet.getClaim(USER_ID);
        return new JWTToken(userId);
    }
}
