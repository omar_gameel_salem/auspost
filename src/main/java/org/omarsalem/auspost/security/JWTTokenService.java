package org.omarsalem.auspost.security;

import org.omarsalem.auspost.exceptions.AuthenticationException;

public interface JWTTokenService {
    String createToken(Long userId);

    JWTToken verifyToken(String token) throws AuthenticationException;
}
