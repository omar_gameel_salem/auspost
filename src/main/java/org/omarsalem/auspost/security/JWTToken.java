package org.omarsalem.auspost.security;

import lombok.Data;

@Data
public class JWTToken {
    private Long id;

    public JWTToken(Long id) {
        this.id = id;
    }
}
