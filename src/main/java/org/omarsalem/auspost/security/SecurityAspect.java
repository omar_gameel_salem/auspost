package org.omarsalem.auspost.security;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class SecurityAspect {

    public static final String AUTH_HEADER_NAME = "Authorization";
    private final JWTTokenService jwtTokenService;

    public SecurityAspect(JWTTokenService jwtTokenService) {
        this.jwtTokenService = jwtTokenService;
    }

    @Around("execution(public * *(..)) && @annotation(authenticated)")
    public Object process(ProceedingJoinPoint joinPoint, Authenticated authenticated) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        final String token = request.getHeader(AUTH_HEADER_NAME);
        jwtTokenService.verifyToken(token);
        return joinPoint.proceed();
    }
}