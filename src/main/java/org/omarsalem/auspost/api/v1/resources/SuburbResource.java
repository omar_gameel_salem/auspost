package org.omarsalem.auspost.api.v1.resources;

import io.swagger.annotations.Api;
import org.omarsalem.auspost.api.v1.models.Suburb;
import org.omarsalem.auspost.api.v1.services.SuburbService;
import org.omarsalem.auspost.exceptions.NotFoundException;
import org.omarsalem.auspost.exceptions.ValidationException;
import org.omarsalem.auspost.security.Authenticated;
import org.omarsalem.auspost.security.SecurityAspect;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "Suburb")
@RequestMapping(ApiEndpoints.Suburb.BASE)
public class SuburbResource {
    private final SuburbService suburbService;

    public SuburbResource(SuburbService suburbService) {
        this.suburbService = suburbService;
    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = ApiEndpoints.Suburb.GET_BY_NAME, produces = MediaType.APPLICATION_JSON_VALUE)
    public Suburb getByName(@PathVariable String name) throws NotFoundException {
        return suburbService.getByName(name);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = ApiEndpoints.Suburb.GET_BY_POSTCODE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Suburb getByPostCode(@PathVariable String postcode) throws NotFoundException {
        return suburbService.getByPostCode(postcode);
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Authenticated
    public Suburb add(@RequestBody Suburb suburb,
                      @RequestHeader(value = SecurityAspect.AUTH_HEADER_NAME) String token) throws ValidationException {
        return suburbService.add(suburb);
    }
}
