package org.omarsalem.auspost.api.v1.resources;

public class ApiEndpoints {
    public static final String BASE_URL = "/api";
    public static final String V1 = "/v1";

    public class Suburb {
        public static final String BASE = BASE_URL + V1 + "/suburbs";
        public static final String GET_BY_NAME = "/name/{name}";
        public static final String GET_BY_POSTCODE = "/postcode/{postcode}";
    }
}
