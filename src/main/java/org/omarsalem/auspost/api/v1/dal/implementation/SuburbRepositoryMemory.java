package org.omarsalem.auspost.api.v1.dal.implementation;

import org.omarsalem.auspost.api.v1.dal.contract.SuburbRepository;
import org.omarsalem.auspost.api.v1.models.Suburb;
import org.omarsalem.auspost.exceptions.NotFoundException;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Repository;

import java.util.Locale;
import java.util.Optional;
import java.util.TreeMap;

@Repository
public class SuburbRepositoryMemory implements SuburbRepository, MessageSourceAware {
    private final TreeMap<String, Suburb> suburbs = new TreeMap<>();
    private MessageSource messageSource;

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public Suburb getByName(String name) throws NotFoundException {
        final Suburb suburb = suburbs.get(name);
        if (suburb == null) {
            final String message = messageSource.getMessage("suburb.not.exists", null, Locale.ENGLISH);
            throw new NotFoundException(message);
        }
        return suburb;
    }

    @Override
    public Suburb getByPostCode(String postCode) throws NotFoundException {
        final Optional<Suburb> optionalSuburb = suburbs
                .values()
                .stream()
                .filter(s -> s.getPostCode().equals(postCode))
                .findAny();
        if (!optionalSuburb.isPresent()) {
            final String message = messageSource.getMessage("suburb.not.exists", null, Locale.ENGLISH);
            throw new NotFoundException(message);
        }
        return optionalSuburb.get();
    }

    @Override
    public Suburb add(Suburb suburb) {
        Long id = 1l;
        if (!suburbs.isEmpty()) {
            id = suburbs.
                    lastEntry()
                    .getValue()
                    .getId() + 1;
        }
        suburb.setId(id);
        suburbs.put(suburb.getName(), suburb);
        return suburb;
    }
}
