package org.omarsalem.auspost.api.v1.models;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;

@Data
public class Suburb {
    private Long id;
    @NotBlank(message = "{suburb.invalid.name}")
    @Pattern(regexp = "[a-zA-Z]+", message = "{suburb.invalid.name}")
    private String name;

    @Pattern(regexp = "\\d{4}", message = "{suburb.invalid.postcode}")
    @NotNull(message = "{suburb.invalid.postcode}")
    private String postCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Suburb suburb = (Suburb) o;
        return
                Objects.equals(name, suburb.name) &&
                        Objects.equals(postCode, suburb.postCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, postCode);
    }
}
