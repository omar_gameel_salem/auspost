package org.omarsalem.auspost.api.v1.dal.contract;

import org.omarsalem.auspost.api.v1.models.Suburb;
import org.omarsalem.auspost.exceptions.NotFoundException;

public interface SuburbRepository {
    Suburb getByName(String name) throws NotFoundException;

    Suburb getByPostCode(String postCode) throws NotFoundException;

    Suburb add(Suburb suburb);
}
