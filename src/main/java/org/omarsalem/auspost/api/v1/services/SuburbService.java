package org.omarsalem.auspost.api.v1.services;

import org.omarsalem.auspost.api.v1.dal.contract.SuburbRepository;
import org.omarsalem.auspost.api.v1.models.Suburb;
import org.omarsalem.auspost.exceptions.NotFoundException;
import org.omarsalem.auspost.exceptions.ValidationException;
import org.omarsalem.auspost.utils.ModelValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SuburbService {
    private final SuburbRepository suburbRepository;

    @Autowired
    public SuburbService(SuburbRepository suburbRepository) {
        this.suburbRepository = suburbRepository;
    }

    public Suburb getByName(String name) throws NotFoundException {
        return suburbRepository.getByName(name);
    }

    public Suburb getByPostCode(String postCode) throws NotFoundException {
        return suburbRepository.getByPostCode(postCode);
    }

    public Suburb add(Suburb suburb) throws ValidationException {
        ModelValidator.validate(suburb);
        return suburbRepository.add(suburb);
    }
}
