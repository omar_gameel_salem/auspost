package org.omarsalem.auspost.api.v1.resources;

import lombok.extern.slf4j.Slf4j;
import org.omarsalem.auspost.exceptions.AuthenticationException;
import org.omarsalem.auspost.exceptions.ClientException;
import org.omarsalem.auspost.exceptions.NotFoundException;
import org.omarsalem.auspost.exceptions.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandling {

    @ResponseBody
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<?> handleNotFoundException(NotFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getErrors());
    }

    @ResponseBody
    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<?> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler({ValidationException.class})
    public ResponseEntity<?> handleValidationException(ValidationException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrors());
    }

    @ResponseBody
    @ExceptionHandler({HttpMediaTypeNotSupportedException.class, HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<?> handleUnSupportedRequestExceptions(Exception e) {
        final ClientException clientException = new ValidationException(e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(clientException.getErrors());
    }

    @ResponseBody
    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handleApplicationException(Exception e) {
        log.error(e.toString());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An Error Occured");
    }
}
