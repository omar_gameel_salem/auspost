package org.omarsalem.auspost.utils;

import org.omarsalem.auspost.exceptions.ValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ModelValidator {

    public static <T> void validate(T model) throws ValidationException {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Collection<ConstraintViolation<T>> violations = validator.validate(model);
        if (!violations.isEmpty()) {
            final List<String> errorMessages = violations
                    .stream()
                    .map(ConstraintViolation::getMessage)
                    .collect(Collectors.toList());
            throw new ValidationException(errorMessages);
        }
    }
}
