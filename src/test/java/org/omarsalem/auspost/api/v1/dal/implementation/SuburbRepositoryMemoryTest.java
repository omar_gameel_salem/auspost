package org.omarsalem.auspost.api.v1.dal.implementation;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omarsalem.auspost.AuspostApplication;
import org.omarsalem.auspost.api.v1.models.Suburb;
import org.omarsalem.auspost.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest
@ContextConfiguration(classes = {AuspostApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SuburbRepositoryMemoryTest {

    @Autowired
    private SuburbRepositoryMemory target;

    @Test
    public void getByName_suburbExists() throws NotFoundException {
        //Arrange
        final String name = "maidstone";
        final String postCode = "postCode";
        final Suburb suburb = new Suburb();
        suburb.setName(name);
        suburb.setPostCode(postCode);
        target.add(suburb);

        //Act
        final Suburb result = target.getByName(name);

        //Assert
        Assert.assertEquals(name, result.getName());
        Assert.assertEquals(postCode, result.getPostCode());
    }


    @Test(expected = NotFoundException.class)
    public void getByName_suburbDoesNotExists() throws NotFoundException {
        //Act
        target.getByName("");
    }

    @Test
    public void getByPostCode_suburbExists() throws NotFoundException {
        //Arrange
        final String name = "maidstone";
        final String postCode = "postCode";
        final Suburb suburb = new Suburb();
        suburb.setName(name);
        suburb.setPostCode(postCode);
        target.add(suburb);

        //Act
        final Suburb result = target.getByPostCode(postCode);

        //Assert
        Assert.assertEquals(name, result.getName());
        Assert.assertEquals(postCode, result.getPostCode());
    }


    @Test(expected = NotFoundException.class)
    public void getByPostCode_suburbDoesNotExists() throws NotFoundException {
        //Act
        target.getByPostCode("");
    }

    @Test
    public void add_idAssigned() {
        //Arrange
        final String name = "maidstone";
        final String postCode = "postCode";
        final Suburb suburb = new Suburb();
        suburb.setName(name);
        suburb.setPostCode(postCode);

        //Act
        final Suburb result = target.add(suburb);

        //Assert
        Assert.assertEquals(Long.valueOf(1), result.getId());
    }
}