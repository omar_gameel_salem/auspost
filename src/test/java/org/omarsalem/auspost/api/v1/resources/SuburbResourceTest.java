package org.omarsalem.auspost.api.v1.resources;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.omarsalem.auspost.AuspostApplication;
import org.omarsalem.auspost.api.v1.models.Suburb;
import org.omarsalem.auspost.api.v1.services.SuburbService;
import org.omarsalem.auspost.exceptions.AuthenticationException;
import org.omarsalem.auspost.security.JWTToken;
import org.omarsalem.auspost.security.JWTTokenService;
import org.omarsalem.auspost.security.SecurityAspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {AuspostApplication.class})
public class SuburbResourceTest {
    @MockBean
    private JWTTokenService jwtTokenService;

    @MockBean
    private SuburbService suburbServiceMock;

    @Autowired
    private WebApplicationContext wac;
    private MockMvc api;
    private MvcResult mvcResult;

    @Before
    public void setup() {
        api = MockMvcBuilders.webAppContextSetup(wac).build();
        Mockito.reset(suburbServiceMock);
    }

    @Test
    public void getByName() throws Exception {
        //Arrange
        final String name = "maidstone";
        final String postCode = "3012";
        final Suburb suburb = new Suburb();
        suburb.setName(name);
        suburb.setPostCode(postCode);
        suburb.setId(1l);
        when(suburbServiceMock.getByName(name)).thenReturn(suburb);

        //Act
        final String url = ApiEndpoints.Suburb.BASE + ApiEndpoints.Suburb.GET_BY_NAME.replace("{name}", name);
        mvcResult = api
                .perform(
                        MockMvcRequestBuilders
                                .get(url))
                .andReturn();

        //Assert
        final MockHttpServletResponse response = mvcResult.getResponse();
        Assert.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assert.assertEquals("{\"id\":1,\"name\":\"maidstone\",\"postCode\":\"3012\"}", response.getContentAsString());

    }

    @Test
    public void getByPostCode() throws Exception {
        //Arrange
        final String name = "maidstone";
        final String postCode = "3012";
        final Suburb suburb = new Suburb();
        suburb.setName(name);
        suburb.setPostCode(postCode);
        suburb.setId(1l);
        when(suburbServiceMock.getByPostCode(postCode)).thenReturn(suburb);

        //Act
        final String url = ApiEndpoints.Suburb.BASE + ApiEndpoints.Suburb.GET_BY_POSTCODE.replace("{postcode}", postCode);
        mvcResult = api
                .perform(
                        MockMvcRequestBuilders
                                .get(url))
                .andReturn();

        //Assert
        final MockHttpServletResponse response = mvcResult.getResponse();
        Assert.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assert.assertEquals("{\"id\":1,\"name\":\"maidstone\",\"postCode\":\"3012\"}", response.getContentAsString());
    }

    @Test
    public void add_authenticated() throws Exception {
        //Arrange
        final String name = "maidstone";
        final String postCode = "3012";
        final Suburb suburb = new Suburb();
        suburb.setName(name);
        suburb.setPostCode(postCode);
        final String token = "token";
        when(jwtTokenService.verifyToken(token)).thenReturn(new JWTToken(1l));

        //Act
        mvcResult = api
                .perform(
                        MockMvcRequestBuilders
                                .post(ApiEndpoints.Suburb.BASE)
                                .header(SecurityAspect.AUTH_HEADER_NAME, token)
                                .content("{ \"name\": \"" + name + "\", \"postCode\": \"" + postCode + "\"}")
                                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        //Assert
        Assert.assertEquals(HttpStatus.CREATED.value(), mvcResult.getResponse().getStatus());
        verify(suburbServiceMock, times(1)).add(suburb);
    }

    @Test
    public void add_guest_exceptionThrown() throws Exception {
        //Arrange
        final String token = "token";
        when(jwtTokenService.verifyToken(token)).thenThrow(new AuthenticationException("error"));

        //Act
        mvcResult = api
                .perform(
                        MockMvcRequestBuilders
                                .post(ApiEndpoints.Suburb.BASE)
                                .header(SecurityAspect.AUTH_HEADER_NAME, token)
                                .content("{ \"name\": \"name\", \"postCode\": \" postCode \"}")
                                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();


        //Assert
        Assert.assertEquals(HttpStatus.UNAUTHORIZED.value(), mvcResult.getResponse().getStatus());
        verify(suburbServiceMock, never()).add(any(Suburb.class));
    }
}