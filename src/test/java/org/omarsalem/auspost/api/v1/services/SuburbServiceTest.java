package org.omarsalem.auspost.api.v1.services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omarsalem.auspost.AuspostApplication;
import org.omarsalem.auspost.api.v1.dal.contract.SuburbRepository;
import org.omarsalem.auspost.api.v1.models.Suburb;
import org.omarsalem.auspost.exceptions.NotFoundException;
import org.omarsalem.auspost.exceptions.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.mockito.Mockito.*;

@SpringBootTest
@ContextConfiguration(classes = {AuspostApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class SuburbServiceTest {
    @MockBean
    private SuburbRepository suburbRepositoryMock;

    @Autowired
    private SuburbService target;

    @Test
    public void getByName_repoCalled() throws NotFoundException {
        //Arrange
        final String name = "name";
        final Suburb suburb = new Suburb();
        suburb.setName(name);
        when(suburbRepositoryMock.getByName(name)).thenReturn(suburb);

        //Act
        final Suburb actual = target.getByName(name);

        //Assert
        Assert.assertEquals(suburb, actual);
    }

    @Test
    public void getByPostCode_repoCalled() throws NotFoundException {
        //Arrange
        final String postCode = "postCode";
        final Suburb suburb = new Suburb();
        suburb.setPostCode(postCode);
        when(suburbRepositoryMock.getByPostCode(postCode)).thenReturn(suburb);

        //Act
        final Suburb actual = target.getByPostCode(postCode);

        //Assert
        Assert.assertEquals(suburb, actual);
    }

    @Test
    public void add_validModel_repoCalled() throws ValidationException {
        //Arrange
        final Suburb suburb = new Suburb();
        suburb.setName("maidstone");
        suburb.setPostCode("3012");

        //Act
        target.add(suburb);

        //Assert
        verify(suburbRepositoryMock, times(1)).add(suburb);
    }

    @Test
    public void add_faultyModel_repoNotCalled() {
        //Arrange
        final Suburb suburb = new Suburb();
        suburb.setPostCode("invalid");

        //Act
        try {
            target.add(suburb);
        } catch (ValidationException e) {
            final List<String> errors = e.getErrors();
            Assert.assertEquals(2, errors.size());
            Assert.assertTrue(errors.contains("Suburb name should not be empty and characters only"));
            Assert.assertTrue(errors.contains("Suburb postcode should not be empty and only 4 digits"));
        }

        //Assert
        verify(suburbRepositoryMock, never()).add(suburb);
    }
}